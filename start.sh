#!/bin/bash

set -eu -o pipefail

mkdir -p /app/data/_data_/_default_/domains/ /app/data/_data_/_default_/configs/ /run/rainloop/sessions

mail_domains="${CLOUDRON_MAIL_DOMAINS:-$CLOUDRON_EMAIL_DOMAINS}"
# compat for env var change in next release
[[ -z ${CLOUDRON_EMAIL_IMAP_SERVER:-} ]] && CLOUDRON_EMAIL_IMAP_SERVER=$CLOUDRON_MAIL_IMAP_SERVER
[[ -z ${CLOUDRON_EMAIL_IMAPS_PORT:-} ]] && CLOUDRON_EMAIL_IMAPS_PORT=$CLOUDRON_MAIL_IMAP_PORT # not a typo
[[ -z ${CLOUDRON_EMAIL_SMTP_SERVER:-} ]] && CLOUDRON_EMAIL_SMTP_SERVER=$CLOUDRON_MAIL_SMTP_SERVER
[[ -z ${CLOUDRON_EMAIL_SMTP_PORT:-} ]] && CLOUDRON_EMAIL_SMTP_PORT=$CLOUDRON_MAIL_SMTP_PORT
[[ -z ${CLOUDRON_EMAIL_SIEVE_SERVER:-} ]] && CLOUDRON_EMAIL_SIEVE_SERVER=$CLOUDRON_MAIL_SIEVE_SERVER
[[ -z ${CLOUDRON_EMAIL_SIEVE_PORT:-} ]] && CLOUDRON_EMAIL_SIEVE_PORT=$CLOUDRON_MAIL_SIEVE_PORT

for domain in $(echo "${mail_domains}" | tr "," "\n"); do
    echo "Generating config for ${domain}"

    # rewrite the config file for the domain
    # We disable smtp secure, since haraka is setup to use insecure auth on local connections
    cat > /app/data/_data_/_default_/domains/${domain}.ini <<EOF
    # WARNING: All settings in this file be overwritten

    imap_host = "${CLOUDRON_EMAIL_IMAP_SERVER}"
    imap_port = ${CLOUDRON_EMAIL_IMAPS_PORT}
    imap_secure = "SSL"
    # ensure login with user@domain
    imap_short_login = Off

    smtp_host = "${CLOUDRON_EMAIL_SMTP_SERVER}"
    smtp_port = ${CLOUDRON_EMAIL_SMTP_PORT}
    smtp_secure = "None"
    smtp_short_login = Off
    smtp_auth = On
    smtp_php_mail = Off

    sieve_use = On
    sieve_allow_raw = Off
    sieve_host = "${CLOUDRON_EMAIL_SIEVE_SERVER}"
    sieve_port = ${CLOUDRON_EMAIL_SIEVE_PORT}
    # this enables STARTTLS
    sieve_secure = "TLS"

    # List of domain users webmail is allowed to access. Use a space as delimiter.
    white_list = ""
EOF

done

# create or update initial settings
default_domain="${CLOUDRON_MAIL_DOMAIN:-$CLOUDRON_EMAIL_DOMAIN}"
settings_file="/app/data/_data_/_default_/configs/application.ini"
if [[ ! -f "${settings_file}" ]]; then
    cat > "${settings_file}" <<EOF
[webmail]
title = "Cloudron Webmail"
loading_description = "Cloudron Webmail"
allow_user_background = On
attachment_size_limit = 25

[security]
allow_admin_panel = Off
openpgp = On

[login]
default_domain = "${default_domain}"
forgot_password_link_url = ""

[contacts]
enable = On
allow_sync = On

[labs]
smtp_use_auth_plain = Off
EOF
fi

## Overwrite dynamic settings
# check if smtp_use_auth_plain is already mentioned
if grep -q "smtp_use_auth_plain" "${settings_file}"; then
    sed -e "s,smtp_use_auth_plain = .*,smtp_use_auth_plain = Off," -i "${settings_file}"
else
    sed -e "s,\[labs\],\[labs\]\nsmtp_use_auth_plain = Off," -i "${settings_file}"
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

chown -R www-data.www-data /run/rainloop /app/data

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

